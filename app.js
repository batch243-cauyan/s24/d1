// console.log("Hello World!");

//[section] Exponent operator;
// before ES6 
    const firstNum = 8**2;
    console.log(firstNum);

// ES66
    const secondNum = Math.pow(8,2);
    console.log(secondNum);

    // Using template literal
    // using backticks(``);
 let name = "John";
    let message = `Hello ${name}! Welcome to Programming!`;
    console.log(message);

    // Template literals allow us to write strings with embedded JavaScript

    const interestRate = 0.1;
    const principal = 1000;
    console.log(`The interest on your savings account is: ${interestRate*principal}`);

    //[Section] Array destructuring
        /*
            -allows us to unpack elements in arrays into distinct variables.
            -allows us to name array elements with variables instead of using index numbers.
            -it will help us with code readability.
            syntax:
            let/const [variableNameA, variableNameB] = arrayName;
        */

    const fullName = ["Juan", "Dela", "Cruz"];
    //Before array destructuring

    console.log(fullName[0]);
    console.log(fullName[1]);
    console.log(fullName[2]);
    console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}!  It's nice to meet you.`);
    //Array Destructuring 

    const [firstName, middleName, lastName] = fullName;
    console.log(firstName);
    console.log(middleName);
    console.log(lastName);

    console.log(fullName);
    console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

    //[section] Object Destructuring
    /*
        -allows us to unpack properties of objects into distinct variables.
        -syntax:
            let/const {propertyNameA, propertyNameB, propertyNameC} = objectName;

    */

    const person = {
        givenName: "Jane",
        maidenName: "Dela",
        familyName: "Cruz",
        
    }

 //Before the object destructuring
    console.log(person);
    console.log(person.givenName);
    console.log(person['familyName']);

// Object destructuring : creates variables from existintg object properties.
        // variables must be the same name of the keys.
            
    let {givenName, maidenName, familyName} = person;

    console.log(givenName);
    console.log(maidenName);
    console.log(familyName);


function getFullName({givenName, maidenName, familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}` )
}

getFullName(person);


// [Section] Arrow Functions
    // Compact alternative syntax to traditional functions.
    // useful for code snippets where creating functions will not be reused in any other portion of the code.

    const hello = ()=>{
        console.log("Hello World!");
    }

    hello();

    e = ()=>console.log("Hello World");
    e();

    // before arrow function and template literals
       function printFullName(firstName, middleInitial, lastName){
            console.log(firstName+ " " + middleInitial + ' ' + lastName);

       }

       printFullName("Chris", "O.", "Mortel");

       let fullName2 = (firstName, middleInitial, lastName) =>{
            console.log(`${firstName} ${middleInitial} ${lastName}`);
       }

       fullName2("Chris","O.","Mortel");

    //    Arrow functions with loops

    const students = ["John", "Jane", "Judy"];

    // before arrow function 

    function iterate(student){
        console.log(student + " is a student!");
    }

    students.forEach(iterate);


    
    let students2 = student =>{
        console.log(`${student} is a student!`);
    }

    const names = ["Hilda", "Kramer", "Sheild"];

    //[Section] Implicit Return statement
        /*
            There are instances when you can omit return statement
            this works because even without return statement
            Javascript implicitly adds it for the result of the function.

        */
            const add = (x,y) =>{
                console.log(x+y);
                x+y;
            }

           let sum =  add(23,45); // doesnt return value.
           console.log(sum);

           const add2 = (x,y) =>{
            console.log(x+y);
            return x+y;
        }

       let sum2 =  add2(23,45); // doesnt return value.
       console.log(sum2);


       const subtract = (x,y) => x-y; // this returns a value.
       subtract(10,5);
       let difference = subtract(10,5);
       console.log(difference);

       //[section] Default Function Argument Value
       //    provide a default argument value if none is provided.
       // when the function is invoked.

       const greet =(name = "John") =>{
            return `Good morning, ${name}!`;
       }

       console.log(greet());

       //[Section] Class-based Object Blueprints

       //Allows us to create/instantiation of objects using classes blueprints.

    //    Creating class
    //    constructor is a special method of a class for creating/initializing an object for the class.

    /*
       syntax:
        class className{
                constructor (objectValueA, objectValueB, ...){
                    this.objectPropertyA = objectValue A;
                    this.objectPropertyB = objectValue A;
                    
                }
        }
*/

class Car{
    constructor(brand, name, year){
        this.carBrand  = brand;
        this.carName = name;
        this.carYear = year;
    }
}

let car1 = new Car("Ford", "Mustang", 2020);
let car2 = new Car("Nissan","aTrave",2015);



console.log(car1);
console.log(car2);
